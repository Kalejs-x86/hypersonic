#pragma GCC optimize "O3,omit-frame-pointer,inline"
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <stack>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;
const bool DEBUG_ENABLED = false;

class Point
{
public:
	int x, y;

	Point() : x(-1), y(-1) { }
	Point(int X, int Y) : x(X), y(Y) { }
	Point(const Point& Pos) : x(Pos.x), y(Pos.y) { }

	void Unset() noexcept { x = y = -1; }
	bool IsSet() const noexcept { return (x >= 0 && y >= 0); }

	bool operator==(const Point& other) const noexcept { return (x == other.x && y == other.y); }
	bool operator!=(const Point& other) const noexcept { return (x != other.x || y != other.y); }
	Point& operator+=(const Point& other) { this->x += other.x; this->y += other.y; return *this; }
	Point operator+(const Point& other) { return Point(this->x + other.x, this->y + other.y); }
	friend ostream& operator<<(ostream& stream, const Point& point) {
		stream << "(" << point.x << ", " << point.y << ")";
		return stream;
	}
};

class CPath
{
public:
	deque<Point> Steps;
	Point Start, End;

	CPath() {}
	CPath(const deque<Point>& Path, const Point& StartPos, const Point& EndPos) : Steps{ Path }, Start{ StartPos }, End{ EndPos } {}
	CPath(const Point& StartPos, const Point& EndPos) : Start{ StartPos }, End{ EndPos } {}
	CPath(const CPath& other) : CPath(other.Steps, other.Start, other.End) {}

	Point NextStep() {
		if (Steps.size() > 0)
		{
			Point Step = Steps.front();
			if (Steps.size() > 1)
				Steps.pop_front();
			return Step;
		}
		else return Point(-66, -66);
	}

	void AddStep(const Point& Pos)
	{
		Steps.push_front(Pos);
		if (End.IsSet() == false)
			End = Steps.back();
		if (Start.IsSet() == false)
			Start = Steps.front();
	}
	unsigned Distance() const noexcept { return Steps.size(); }
	bool IsSet() const noexcept { return Steps.size() != 0; }
	void Clear() noexcept { Steps.clear(); End = Point(); }
};

namespace DEBUG
{
	template <typename T>
	void Print(string Name, T V) { cerr << Name << " = " << V; }
	template <typename It>
	void Print(string Name, It Begin, It End, unsigned ItemsToPrint = 0) {
		cerr << Name << "(" << Name.size() << ")\n";
		for (unsigned i = 0; i < ItemsToPrint && Begin != End;)
		{
			cerr << "\t#" << i + 1 << " " << *Begin << endl;
			Begin++;
			if (ItemsToPrint != 0)
				i++;
		}
	}
}

class CEntity
{
protected:
	int m_iType = 0;
	int m_iOwner = 0;
	Point m_Pos;
	int m_iParam1 = 0;
	int m_iParam2 = 0;
public:
	CEntity() {}

	CEntity(int Type, int Owner, int X, int Y, int Param1, int Param2) :
		m_iType(Type), m_iOwner(Owner), m_Pos(Point(X, Y)), m_iParam1(Param1), m_iParam2(Param2) {}

	enum EntityType {
		Player,
		Bomb,
		Item,
		ErrorEntity
	};

	EntityType GetType() const noexcept {
		switch (m_iType)
		{
		default: return ErrorEntity;
		case 0: return Player;
		case 1: return Bomb;
		case 2: return Item;
		}
	}

	bool IsPassable() const noexcept {
		switch (GetType())
		{
		case Bomb: return false;
		default: return true;
		}
	}

	int GetOwner() const noexcept { return m_iOwner; }
	Point GetPos() const noexcept { return m_Pos; }
};

class CTile
{
public:
	enum TileType {
		Empty,
		Box,
		Wall
	};
private:
	TileType m_Type;
	Point m_Pos;
	CEntity* m_pEntity = nullptr;
	bool m_bExploding = false; // Will be exploding soon
public:
	CTile() : m_Type(Empty) {}
	CTile(unsigned uType) : m_Type(static_cast<TileType>(uType)) { }
	CTile(Point Pos, TileType Type) : m_Pos(Pos), m_Type(Type) { };

	TileType GetType() const noexcept { return m_Type; }
	void SetType(TileType Type) noexcept { m_Type = Type; }
	char GetChar() const noexcept {
		switch (m_Type) {
		case Empty: return '.';
		case Box: return '0';
		case Wall: return 'X';
		default: return '?';
		}
	}
	void SetExplode() noexcept { m_bExploding = true; }
	bool WillExplode() const noexcept { return m_bExploding; }
	bool IsSolid() const noexcept { return (m_Type == Wall || m_Type == Box); }
	bool IsBreakable() const noexcept { return m_Type == Box; }
	void SetEmpty() noexcept { m_Type = Empty; }
	Point GetPos() const noexcept { return m_Pos; }
	void SetPos(int x, int y) noexcept { m_Pos = Point(x, y); };
	void SetPos(const Point& P) noexcept { m_Pos = P; };
	CEntity* GetEntity() const noexcept { return m_pEntity; }
	void SetEntity(CEntity* pEntity) { m_pEntity = pEntity; }
};

class CPlayer : public CEntity
{
public:
	CPlayer() {}
	CPlayer(CEntity &Ent) : CEntity(Ent) { }

	int GetID() const noexcept { return m_iOwner; }
	int GetRemainingBombs() const noexcept { return m_iParam1; }
	int GetExplosionRange() const noexcept { return m_iParam2; }
};

class CBomb : public CEntity
{
public:
	CBomb(CEntity &Ent) : CEntity(Ent) {}
	CBomb(Point Pos, int Owner, int ExplosionRange, int RemainingTime = 8) {
		m_Pos = Pos;
		m_iOwner = Owner;
		m_iParam2 = ExplosionRange;
		m_iParam1 = RemainingTime;
	}

	int GetRemainingTime() const noexcept { return m_iParam1; }
	void SetRemainingTime(int TimeLeft) noexcept { m_iParam1 = TimeLeft; }
	int GetExplosionRange() const noexcept { return m_iParam2; }
};

class CItem : public CEntity
{
public:
	CItem(CEntity &Ent) : CEntity(Ent) {}
	int GetType() const noexcept { return m_iParam1; }
};

class CExplosion
{
private:
	Point m_Pos;
	unsigned m_RemainingTurns = 0;
public:
	CExplosion() {}
	CExplosion(Point Pos, unsigned Turns) : m_Pos(Pos), m_RemainingTurns(Turns) {}

	Point GetPos() const noexcept { return m_Pos; }
	unsigned GetTurns() const noexcept { return m_RemainingTurns; }
	void SetTurns(unsigned TurnsLeft) noexcept { m_RemainingTurns = TurnsLeft; }
};

class CGameWorld
{
private:
	class C_AStar
	{
		static unsigned Distance(Point A, Point B) {
			unsigned Dist = 0;
			Dist += abs(A.x - B.x);
			Dist += abs(A.y - B.y);
			return Dist;
		}

		struct Node {
			shared_ptr<Node> Parent;
			unsigned GCost;
			unsigned HCost;
			Point Pos;
			Node(shared_ptr<Node> ParentNode, Point Position, Point Start, Point End) : Pos{ Position } {
				Parent = ParentNode;
				GCost = Distance(Position, Start);
				HCost = Distance(Position, End);
			}
			unsigned FCost() { return GCost + HCost; }
		};

		typedef shared_ptr<Node> NodePtr;

		const CGameWorld& World;
		Point Start, End;

		deque<NodePtr> GetNeighbours(NodePtr Parent)
		{
			deque<NodePtr> Result;
			auto fCriteria = [this](const CTile& Tile) {
				return World.IsPassable(Tile.GetPos());
			};

			auto Tiles = World.GetAdjacentTiles(Parent->Pos, 1, fCriteria);
			for (auto& Tile : Tiles) {
				Result.emplace_back(make_shared<Node>(Parent, Tile.GetPos(), Start, End));
			}
			return Result;
		}
	public:
		C_AStar(const CGameWorld& W, Point S, Point E) : World{ W }, Start{ S }, End{ E } {}

		bool FindPath(CPath& Path)
		{
			deque<NodePtr> OpenSet;
			deque<NodePtr> ClosedSet;
			NodePtr PathEnd = nullptr;

			auto IsNodeClosed = [&ClosedSet](NodePtr N) {
				for (NodePtr OpenN : ClosedSet) {
					if (OpenN->Pos == N->Pos)
						return true;
				}
				return false;
			};

			auto IsNodeInOpen = [&OpenSet](NodePtr& N) {
				for (NodePtr OpenN : OpenSet) {
					if (OpenN->Pos == N->Pos)
					{
						N = OpenN;
						return true;
					}
				}
				return false;
			};

			OpenSet.emplace_back(make_shared<Node>(nullptr, Start, Start, End));

			while (OpenSet.size() > 0)
			{
				NodePtr CurNode = OpenSet.front();
				OpenSet.pop_front();
				ClosedSet.push_back(CurNode);

				if (CurNode->Pos == End)
				{
					PathEnd = CurNode; // End found
					break;
				}

				deque<NodePtr> Neighbours = GetNeighbours(CurNode);
				for (NodePtr N : Neighbours)
				{
					if (IsNodeClosed(N) == false)
					{
						unsigned MoveCost = CurNode->GCost + Distance(CurNode->Pos, N->Pos);
						bool IsNewNode = !IsNodeInOpen(N);
						if (MoveCost < N->GCost || IsNewNode)
						{
							N->GCost = MoveCost;
							N->Parent = CurNode;
							if (IsNewNode)
								OpenSet.push_back(N);
						}
					}
				}
			}

			if (PathEnd != nullptr)
			{
				NodePtr CurNode = PathEnd;
				CPath Result(Start, End);

				while (CurNode)
				{
					if (CurNode->Parent != nullptr) // Not starting node
						Result.AddStep(CurNode->Pos);
					CurNode = CurNode->Parent;
				}

				Path = Result;
				return true;
			}
			else
				return false;
		}
	};

	static const unsigned short DEFAULT_WIDTH = 20U;
	static const unsigned short DEFAULT_HEIGHT = 20U;
	CTile m_TileMap[DEFAULT_WIDTH][DEFAULT_HEIGHT];

	unsigned m_uWidth = 0; // Actual map sizes
	unsigned m_uHeight = 0;
	vector<CEntity> m_Entities;
	vector<CExplosion> m_Explosions;

	enum PlayerState {
		IDLE,
		GO_BOMBSITE,
		GO_SAFETY,
		WAIT_BOMBSITE,
		WAIT_SAFETY
	} m_Action = IDLE; // What the player is doing ATM
	int m_iLocalID = 0; // Local player ID
	CPath m_Target; // Next target (bomb placement, safe spot, etc)
	CPlayer m_LocalPlayer;
	Point m_PlayerPos; // Where our player is
	bool m_bNoBoxes;

	static const unsigned MAX_DISTANCE = 0xFFFFFFFF;
public:
	bool IsTileValid(const Point& Pos) const noexcept { return (Pos.x >= 0 && Pos.x < m_uWidth && Pos.y >= 0 && Pos.y < m_uHeight); };
	bool IsTileValid(int x, int y) const noexcept { return (x >= 0 && x < m_uWidth && y >= 0 && y < m_uHeight); };
	const CTile& TileAt(const Point& Pos) const noexcept { return m_TileMap[Pos.x][Pos.y]; }
	const CTile& TileAt(int x, int y) const noexcept { return m_TileMap[x][y]; }
	bool IsPassable(const Point& Pos) const {
		auto& Tile = TileAt(Pos);
		if (Tile.IsSolid()) return false;
		auto TileEnt = Tile.GetEntity();
		if (TileEnt != nullptr)
		{
			return TileEnt->IsPassable();
		}
		return true;
	}
	bool WillTileExplode(const Point& P, unsigned Turns = 0, CExplosion * const pExplosion = nullptr) const
	{
		auto Expl = find_if(m_Explosions.begin(), m_Explosions.end(), [&P](const CExplosion& Expl) { return Expl.GetPos() == P; });
		if (Expl != m_Explosions.end())
		{
			if (Turns != 0)
			{
				if ((Expl->GetTurns()) == Turns)
				{
					if (pExplosion) *pExplosion = *Expl;
					return true; // Turns match, will explode
				}
				else
					return false; // Turns dont match
			}
			return true; // Turns not specified, found explosion at Point P
		}
		return false; // Explosion not found
	}

	enum Cardinal
	{
		North,
		East,
		South,
		West,
		Any
	};

	vector<CTile> GetAdjacentTiles(Point Origin, unsigned Distance, function<bool(const CTile&)> fCriteria = 0, Cardinal Direction = Any) const
	{
		bool PathEnd[4] = { false, false, false, false };
		if (Direction != Any)
		{
			for (auto& Path : PathEnd) {
				Path = true; // Set all paths to ignored
			}

			PathEnd[Direction] = false; // Search only selected path
		}

		auto IsMatch = [this, &fCriteria](Point& Pos) {
			if (IsTileValid(Pos)) // Check coordinates
			{
				if (fCriteria != 0) // Is criteria specified?
				{
					if (fCriteria(TileAt(Pos))) // Does it match criteria?
						return true; // Matches criteria
					else
						return false; // Doesnt match criteria
				}
				else
					return true; // No criteria, add whatever
			}
			return false; // Out of bounds
		};

		vector<CTile> Result;

		for (int Offset = 1; Offset <= Distance; ++Offset)
		{
			if (PathEnd[North] == false)
			{
				Point Difference(0, -Offset);
				Point CurPos = Origin + Difference;
				if (IsMatch(CurPos))
					Result.push_back(TileAt(CurPos));
				else
					PathEnd[North] = true;
			}

			if (PathEnd[South] == false)
			{
				Point Difference(0, Offset);
				Point CurPos = Origin + Difference;
				if (IsMatch(CurPos))
					Result.push_back(TileAt(CurPos));
				else
					PathEnd[South] = true;
			}

			if (PathEnd[West] == false)
			{
				Point Difference(-Offset, 0);
				Point CurPos = Origin + Difference;
				if (IsMatch(CurPos))
					Result.push_back(TileAt(CurPos));
				else
					PathEnd[West] = true;
			}

			if (PathEnd[East] == false)
			{
				Point Difference(Offset, 0);
				Point CurPos = Origin + Difference;
				if (IsMatch(CurPos))
					Result.push_back(TileAt(CurPos));
				else
					PathEnd[East] = true;
			}
		}

		return Result;
	}

	bool IsPathAccessible(const CPath& Path) const
	{
		if (WillTileExplode(Path.End))
			return false;

		unsigned Turns = 2;
		for (Point Step : Path.Steps)
		{
			if (Step == m_PlayerPos) continue;

			auto Tile = TileAt(Step);
			auto TileEnt = Tile.GetEntity();

			if (TileAt(Step).IsSolid())
				return false;

			if (TileEnt != nullptr)
			{
				if (TileEnt->IsPassable() == false)
					return false;
			}

			if (WillTileExplode(Step, Turns))
				return false;
			Turns++;
		}

		return true;
	}

	bool IsPathAccessible(Point Start, Point End, CPath * const pPath = nullptr) const
	{
		if (TileAt(End).IsSolid()) return false;

		// Get path to end
		C_AStar AStar(*this, Start, End);
		CPath Path;
		if (AStar.FindPath(Path) == false)
			return false;

		if (pPath)
			*pPath = Path;

		return IsPathAccessible(Path);
	}

	unsigned GetDist(Point A, Point B) const
	{
		if (A == B) return 0;

		C_AStar AStar(*this, A, B);
		CPath Path;

		bool PathFound = AStar.FindPath(Path);
		if (PathFound)
			return Path.Distance();
		else
			return MAX_DISTANCE;
	}

	vector<CExplosion> SimulateExplosion(const CBomb& Bomb, float * const pScore = nullptr) const
	{
		vector<CExplosion> Explosions;
		unsigned BoxesDestroyed = 0;
		unsigned PlayersKilled = 0;

		auto AddExplosion = [&Explosions, &Bomb](Point Pos) {
			Explosions.emplace_back(CExplosion(Pos, Bomb.GetRemainingTime()));
		};

		auto WillStopExplosion = [this](const CTile& Tile) {
			if (Tile.IsBreakable()) return true;
			auto Ent = Tile.GetEntity();

			if (Ent != nullptr)
			{
				if (Ent->GetType() != CEntity::Player)
					return true; // Entities will also stop the explosion but ignore players because they are unpredictable
			}

			return false; // Exprosion will continue!
		};

		Point BombPos = Bomb.GetPos();
		auto Criteria = [](const CTile& Tile) {
			return (Tile.GetType() != CTile::Wall); // Only get tiles which explosions can go through
		};

		// Mark tile where bomb is placed
		AddExplosion(BombPos);

		for (unsigned Direction = 0; Direction < 4; ++Direction)
		{
			auto Tiles = GetAdjacentTiles(BombPos, Bomb.GetExplosionRange() - 1, Criteria, static_cast<Cardinal>(Direction));

			for (auto& Tile : Tiles)
			{
				AddExplosion(Tile.GetPos());

				if (Tile.GetType() == CTile::Box && Tile.WillExplode() == false)
					BoxesDestroyed++;

				// This is in case of boxes, items, and other ents
				// Explosion will not continue onward but it will affect the stopping tile
				if (WillStopExplosion(Tile))
					break;
			}
		}

		vector<CPlayer> Players;
		for (auto Ent : m_Entities)
		{
			if (Ent.GetType() == CEntity::Player && Ent.GetOwner() != m_iLocalID)
				Players.emplace_back(CPlayer(Ent));
		}

		/*
		// Ignore for now
		for (auto& Expl : Explosions)
		{
		for (auto& Player : Players)
		{
		if (Expl.GetPos() == Player.GetPos())
		PlayersKilled++;
		}
		}
		*/

		if (pScore)
			*pScore = (BoxesDestroyed * 1.5f) + PlayersKilled;
		return Explosions;
	}

	const CPlayer& GetPlayer(int PlayerID) const
	{
		if (PlayerID == m_iLocalID) return m_LocalPlayer;
		for (unsigned i = 0; i < m_Entities.size(); ++i)
		{
			const CEntity& Ent = m_Entities[i];
			if (Ent.GetType() == CEntity::Player && Ent.GetOwner() == PlayerID)
			{
				return static_cast<const CPlayer&>(Ent);
			}
		}
	}

	deque<CPath> CalcOptimalPlacements() const
	{
		deque<pair<float, CPath>> Placements;
		deque<CPath> Result;

		for (int y = 0; y < m_uHeight; ++y)
		{
			for (int x = 0; x < m_uWidth; ++x)
			{
				Point CurPos(x, y);
				CPath Path;

				if (CurPos == m_PlayerPos) continue;
				if (IsPathAccessible(m_PlayerPos, CurPos, &Path) == false) continue;

				CBomb FakeBomb(CurPos, m_iLocalID, m_LocalPlayer.GetExplosionRange());
				float Score = 0;
				SimulateExplosion(FakeBomb, &Score);

				if (Score != 0)
				{
					for (auto& Step : Path.Steps)
					{
						auto TileEnt = TileAt(Step).GetEntity();
						if (TileEnt && TileEnt->GetType() == CEntity::Item)
							Score += 0.5f;
					}
					float Distance = static_cast<float>(Path.Distance());
					if (Distance == 0.0f)
						Distance = 0.5f; // To avoid divide by zero
					float TotalScore = Score / Distance;
					Placements.emplace_back(TotalScore, Path);
				}
			}
		}

		if (Placements.size() > 1) {
			// Sort vector by most optimal (most score)
			sort(Placements.begin(), Placements.end(), [](auto& left, auto& right) {
				return left.first > right.first;
			});
		}

		for (auto& P : Placements)
		{
			Result.push_back(P.second);
		}

		return Result;
	}

	void UpdatePlayerPos()
	{
		for (auto Ent : m_Entities)
		{
			if (Ent.GetType() == CEntity::Player && Ent.GetOwner() == m_iLocalID)
			{
				m_LocalPlayer = static_cast<CPlayer>(Ent);
				m_PlayerPos = Ent.GetPos();
				break;
			}
		}
	}

	Point GetClosestItem() const
	{
		unsigned BestDistance = MAX_DISTANCE;
		Point ClosestItem(-77, -77);
		for (auto& Ent : m_Entities)
		{
			if (Ent.GetType() == CEntity::Item)
			{
				Point ItemPos = Ent.GetPos();
				CPath Path;
				if (!WillTileExplode(ItemPos) && IsPathAccessible(m_PlayerPos, ItemPos, &Path))
				{
					if (Path.Distance() < BestDistance)
					{
						ClosestItem = ItemPos;
						BestDistance = Path.Distance();
					}
				}
			}
		}

		return ClosestItem;
	}

	CPath GetSafeTile() const
	{
		CPath Result;
		// If the player is not in danger, safest tile is where player is standing
		bool CurTileSafe = !WillTileExplode(m_PlayerPos);

		if (CurTileSafe)
		{
			Result.AddStep(m_PlayerPos);
			return Result;
		}

		// Current tile isnt safe, get surrounding tiles
		unsigned BestDistance = MAX_DISTANCE;
		for (unsigned y = 0; y < m_uHeight; ++y)
		{
			for (unsigned x = 0; x < m_uWidth; ++x)
			{
				Point CurPos = Point(x, y);
				auto Tile = TileAt(CurPos);
				auto TileEnt = Tile.GetEntity();

				if (Tile.IsSolid()) continue;
				if (TileEnt != nullptr && !TileEnt->IsPassable()) continue;

				CPath Path;
				if (IsPathAccessible(m_PlayerPos, CurPos, &Path))
				{
					if (Path.Distance() < BestDistance)
					{
						BestDistance = Path.Distance();
						Result = Path;
					}
				}
			}
		}

		if (DEBUG_ENABLED && Result.End == m_PlayerPos) cerr << "ERROR: Failed to find safe tile\n";
		return Result;
	}

	CGameWorld(int Width, int Height, int LocalPlayerID) :
		m_uWidth(Width), m_uHeight(Height), m_iLocalID(LocalPlayerID) { }

	void UpdateWorld(const vector<string>& Inputs, const vector<CEntity>& Ents)
	{
		if (Inputs.size() == 0) return;
		m_Entities = Ents;
		UpdatePlayerPos();

		m_bNoBoxes = true;
		// Set tile information
		for (unsigned y = 0; y < m_uHeight && y < Inputs.size(); ++y)
		{
			string CurLine = Inputs[y];
			for (unsigned x = 0; x < m_uWidth && x < CurLine.length(); ++x)
			{
				char CurChar = CurLine[x];
				if (CurChar == '.')
					m_TileMap[x][y] = { Point(x, y), CTile::Empty };
				else if (CurChar == 'X')
					m_TileMap[x][y] = { Point(x, y), CTile::Wall };
				else
				{
					m_TileMap[x][y] = { Point(x, y), CTile::Box };
					m_bNoBoxes = false;
				}
			}
		}

		// Bind entities to tiles
		for (unsigned i = 0; i < m_Entities.size(); ++i)
		{
			CEntity& Ent = m_Entities[i];
			Point Pos = Ent.GetPos();
			m_TileMap[Pos.x][Pos.y].SetEntity(&m_Entities[i]);
		}
	}

	void NextMove()
	{
		bool PlayerInDanger = false;
		int BombsRemaining = m_LocalPlayer.GetRemainingBombs();
		deque<CPath> Placements;
		if (m_bNoBoxes == false)
			Placements = CalcOptimalPlacements();

		if (DEBUG_ENABLED) cerr << "INFO: Found " << Placements.size() << " placements\n";

		static unsigned short TimeUntilExplosion = 0;
		if (TimeUntilExplosion > 0)
			TimeUntilExplosion--;

		CExplosion Expl;
		if (WillTileExplode(m_PlayerPos, 1, &Expl))
		{
			PlayerInDanger = true;
			TimeUntilExplosion = Expl.GetTurns();
		}

		auto PlayerAtTarget = [this]() {return m_PlayerPos == m_Target.End; };

		auto AddFakeBomb = [this](const Point& Pos) {
			CBomb FakeBomb(Pos, m_iLocalID, m_LocalPlayer.GetExplosionRange());
			auto FakeExplosions = SimulateExplosion(FakeBomb);
			unsigned FakeExplCount = FakeExplosions.size();
			m_Explosions.insert(m_Explosions.end(), FakeExplosions.begin(), FakeExplosions.end());
			return FakeExplCount;
		};

		auto RemoveExplosions = [this](unsigned Count) {
			m_Explosions.erase(m_Explosions.end() - Count, m_Explosions.end());
		};

		auto IsPlacementSafe = [this, &AddFakeBomb, &RemoveExplosions](const Point& Pos) {
			unsigned ExplCount = AddFakeBomb(Pos); // Add hypothetical explosions to explosion set
			auto SafePath = GetSafeTile(); // Do calculations
			RemoveExplosions(ExplCount); // Remove added explosions
			return SafePath.IsSet(); // Can we find a safe path after the hypothetical bomb is dropped?
		};

		bool DropBomb = false;
		auto Move = [](Point Pos, bool Bomb) {
			if (Bomb)
				cout << "BOMB " << Pos.x << " " << Pos.y << endl;
			else
				cout << "MOVE " << Pos.x << " " << Pos.y << endl;
		};

		// Get most optimal explosion points
		auto GetNextPlacement = [this, &Placements, &IsPlacementSafe, &BombsRemaining]() {
			while (Placements.size() > 0)
			{
				CPath Placement = Placements.front();
				Placements.pop_front();
				if ((BombsRemaining == 0 || IsPlacementSafe(Placement.End)) && !WillTileExplode(Placement.End))
					return Placement;
				else if (DEBUG_ENABLED)
				{
					cerr << "INFO: "; DEBUG::Print("Placement", Placement.End); cerr << " is unreachable/unsafe, disregarding\n";
				}
			}

			// No placements are safe/accessible, get nearest safe tile
			if (DEBUG_ENABLED) cerr << "INFO: Placements empty or all placements unsafe, going to safety\n";
			m_Action = GO_SAFETY;
			auto SafePath = GetSafeTile();
			if (SafePath.IsSet() == false)
			{
				SafePath.AddStep(m_PlayerPos);
				m_Action = WAIT_SAFETY;
				TimeUntilExplosion = 1;
			}
			return SafePath;
		};

		// Make sure target is still accessible & safe
		if (m_Target.IsSet() && !IsPathAccessible(m_Target))
		{
			if (DEBUG_ENABLED) cerr << "BOT: Target inaccessible, getting new target\n";
			m_Target = GetNextPlacement();
		}

		if (m_Action == GO_SAFETY && PlayerAtTarget())
			m_Action = WAIT_SAFETY;

		if (m_Action == WAIT_SAFETY)
		{
			if (DEBUG_ENABLED) cerr << "BOT: Waiting in safety for " << TimeUntilExplosion << " turns\n";
			if (TimeUntilExplosion <= 0)
				m_Action = IDLE;
		}

		if (m_Action == IDLE || !m_Target.IsSet())
		{
			if (DEBUG_ENABLED) cerr << "BOT: Nothing to do, going to next target\n";
			m_Target = GetNextPlacement();
			if (m_Action != GO_SAFETY) m_Action = GO_BOMBSITE;
		}

		if (m_Action == WAIT_BOMBSITE)
		{
			if (PlayerInDanger)
			{
				m_Target = GetSafeTile();
				m_Action = GO_SAFETY;
			}
			else
			{
				m_Action = GO_BOMBSITE;
			}
		}

		if (m_Action == GO_BOMBSITE)
		{
			if (PlayerAtTarget())
			{
				if (BombsRemaining > 0)
				{
					AddFakeBomb(m_Target.End);
					m_Target = GetNextPlacement();

					if (DEBUG_ENABLED) { cerr << "BOT: At target, dropping bomb and moving to "; DEBUG::Print("Pos", m_Target.End); cerr << endl; }

					// Drop bomb at bombsite, run to new bombsite
					DropBomb = true;
				}
				else
					m_Action = WAIT_BOMBSITE; // Wait for bombs to regen or for area to clear
			}
			else if (DEBUG_ENABLED)
				cerr << "BOT: Going to bombsite\n";

		}

		// Check if we can drop any bombs along the way
		if (m_Action == GO_BOMBSITE)
		{
			if (BombsRemaining > 1)
			{
				float Destruction = 0.0f;
				CBomb FakeBomb(m_PlayerPos, m_iLocalID, m_LocalPlayer.GetExplosionRange());
				auto FakeExplosions = SimulateExplosion(FakeBomb, &Destruction);
				if (Destruction > 0.0f)
				{
					// Not using IsPlacementSafe because it doesnt make sure that m_Target is still accessible after placement
					m_Explosions.insert(m_Explosions.end(), FakeExplosions.begin(), FakeExplosions.end());
					auto SafePath = GetSafeTile();
					bool IsSafe = SafePath.IsSet() && IsPathAccessible(m_Target);
					// Dont care about m_Explosions cleanup, this is the last check in the algorithm

					if (IsSafe)
					{
						if (DEBUG_ENABLED) cerr << "BOT: Dropping bomb along the way\n";
						DropBomb = true;
					}
				}
			}
		}

		switch (m_Action)
		{
		case GO_BOMBSITE: case GO_SAFETY:
			Move(m_Target.NextStep(), DropBomb); break;
		case WAIT_BOMBSITE: case WAIT_SAFETY:
			Move(m_PlayerPos, DropBomb); break;
		default:
			if (DEBUG_ENABLED) cerr << "BOT: Undefined action, staying still\n";
			Move(m_PlayerPos, false);
		}

	}

	void PredictExplosions()
	{
		// Predict how existing bombs will change the map
		// If a bomb will destroy boxes, then ignore those boxes
		// when calculating placements
		m_Explosions.clear();

		vector<pair<CBomb*, bool>> Bombs; // Bomb pointer, bomb evaluated

		for (int i = 0; i < m_Entities.size(); ++i)
		{
			CEntity* pEnt = &m_Entities[i];
			if (pEnt->GetType() == CEntity::Bomb)
			{
				Bombs.emplace_back(make_pair(reinterpret_cast<CBomb*>(pEnt), false));
			}
		}

		bool NeedResort = true;
		bool AllBombsEvaluated = Bombs.size() == 0;

		while (AllBombsEvaluated == false)
		{
			if (NeedResort)
			{
				// Sort bombs by explosion time
				sort(Bombs.begin(), Bombs.end(), [](auto Pair1, auto Pair2) { return Pair1.first->GetRemainingTime() < Pair2.first->GetRemainingTime(); });
				NeedResort = false;
			}

			for (int i = 0; i < Bombs.size(); ++i)
			{
				if (Bombs[i].second == false)
				{
					auto pBomb = Bombs[i].first;
					auto NewExplosions = SimulateExplosion(*pBomb);

					for (auto& NewExpl : NewExplosions)
					{
						auto ExistingExpl = find_if(m_Explosions.begin(), m_Explosions.end(), [&NewExpl](CExplosion& Existing) {return NewExpl.GetPos() == Existing.GetPos(); });
						Point ExplPos = NewExpl.GetPos();

						if (ExistingExpl != m_Explosions.end())
						{
							// Explosion overlaps
							if (NewExpl.GetTurns() < ExistingExpl->GetTurns())
								ExistingExpl->SetTurns(NewExpl.GetTurns()); // Update the explosion time if its going to explode in less
						}
						else
						{
							m_Explosions.push_back(NewExpl);

							auto TileEnt = TileAt(ExplPos).GetEntity();

							// Chainbombs check
							if (TileEnt != nullptr && TileEnt->GetType() == CEntity::Bomb)
							{
								CBomb* pBomb = reinterpret_cast<CBomb*>(TileEnt);
								if (NewExpl.GetTurns() < pBomb->GetRemainingTime())
								{
									pBomb->SetRemainingTime(NewExpl.GetTurns());
									NeedResort = true;
								}
							}
						}

						if (TileAt(ExplPos).GetType() == CTile::Box)
							m_TileMap[ExplPos.x][ExplPos.y].SetExplode(); // Will explode in the future, ignore in calculations
					}

					m_Explosions.insert(m_Explosions.begin(), NewExplosions.begin(), NewExplosions.end());
					Bombs[i].second = true;

					if (i == Bombs.size() - 1)
						AllBombsEvaluated = true;
				}
			}
		}
	}

	void PrintDebug()
	{
		const char* Actions[] = {
			"IDLE",
			"GO_BOMBSITE",
			"GO_SAFETY",
			"WAIT_BOMBSITE",
			"WAIT_SAFETY"
		};

		auto LocalPlayer = GetPlayer(m_iLocalID);

		DEBUG::Print("Action", string(Actions[m_Action])); cerr << " | ";
		DEBUG::Print("PlayerPos", m_PlayerPos); cerr << " | ";
		DEBUG::Print("Bombs", m_LocalPlayer.GetRemainingBombs()); cerr << " | ";
		DEBUG::Print("Range", m_LocalPlayer.GetExplosionRange()); cerr << endl;

		DEBUG::Print("TargetPos", m_Target.End); cerr << endl;

		/*
		auto Placements = CalcOptimalPlacements();
		cerr << "Placements(" << Placements.size() << ")\n";
		for (unsigned i = 0; i < 3 && i < Placements.size(); ++i)
		{
		cerr << "\t#" << i + 1 << " ";
		DEBUG::Print("Pos", Placements[i].second); cerr << endl;
		}*/
	}
};

/**
* Auto-generated code below aims at helping you parse
* the standard input according to the problem statement.
**/

int main()
{
	int width;
	int height;
	int myId;
	cin >> width >> height >> myId; cin.ignore();

	CGameWorld World(width, height, myId);

	// game loop
	while (1) {
		vector<string> MapInputs;
		vector<CEntity> Entities;

		for (int i = 0; i < height; i++) {
			string row;
			cin >> row; cin.ignore();
			MapInputs.push_back(row);
		}
		int entities;
		cin >> entities; cin.ignore();
		for (int i = 0; i < entities; i++) {
			int entityType;
			int owner;
			int x;
			int y;
			int param1;
			int param2;
			cin >> entityType >> owner >> x >> y >> param1 >> param2; cin.ignore();
			Entities.emplace_back(CEntity(entityType, owner, x, y, param1, param2));
		}

		// Write an action using cout. DON'T FORGET THE "<< endl"
		// To debug: cerr << "Debug messages..." << endl;

		World.UpdateWorld(MapInputs, Entities);
		World.PredictExplosions();
		World.NextMove();
		if (DEBUG_ENABLED) World.PrintDebug();
	}
}